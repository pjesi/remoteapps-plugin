package com.atlassian.labs.remoteapps.host.common.descriptor;

public interface LocalMountBaseUrlResolver
{
    String getLocalMountBaseUrl(String appKey);
}
