package com.atlassian.labs.remoteapps.host.common.service;

import com.atlassian.labs.remoteapps.api.service.SignedRequestHandler;

public interface SignedRequestHandlerServiceFactory extends TypedServiceFactory<SignedRequestHandler>
{
}
