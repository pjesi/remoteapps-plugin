/**
 * This package contains classes used when implementing host environments for version 3 plugins and is not meant to be
 * used by end-user plugins or other plugins providing services to to plugins.
 */
package com.atlassian.labs.remoteapps.host.common;