package com.atlassian.labs.remoteapps.api.service.confluence.domain;

/**
 */
public interface RenderOptions
{
    void setStyle(RenderStyle style);
}
