/**
 * This package contains packages and classes meant to be consumed by those providing services to the Remote Apps
 * Plugin, and are not meant to be consumed by end-user plugins.  This includes things like new permissions, api scopes,
 * or described modules.
 */
package com.atlassian.labs.remoteapps.spi;