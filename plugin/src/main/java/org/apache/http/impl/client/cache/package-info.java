/**
 * This code is copied from https://issues.apache.org/jira/browse/HTTPASYNC-17 and should
 * be removed once the patch is released.
 */
package org.apache.http.impl.client.cache;