package com.atlassian.labs.remoteapps.plugin.integration.plugins;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;

/**
 *
 */
public class I18nModuleDescriptor extends AbstractModuleDescriptor<Void>
{
    @Override
    public Void getModule()
    {
        throw new UnsupportedOperationException();
    }
}
