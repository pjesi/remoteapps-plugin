package com.atlassian.labs.remoteapps.plugin.module.applinks;

import com.atlassian.labs.remoteapps.plugin.module.RemoteModuleGenerator;
import com.atlassian.labs.remoteapps.spi.schema.DocumentBasedSchema;
import com.atlassian.labs.remoteapps.spi.schema.Schema;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Map;

import static java.util.Collections.emptyMap;

/**
 * Creates applink entity types
 */
@Component
public class EntityTypeModuleGenerator implements RemoteModuleGenerator
{
    private final Plugin plugin;

    @Autowired
    public EntityTypeModuleGenerator(PluginRetrievalService pluginRetrievalService)
    {
        this.plugin = pluginRetrievalService.getPlugin();
    }

    @Override
    public String getType()
    {
        return "entity-type";
    }

    @Override
    public String getName()
    {
        return "Entity Type";
    }

    @Override
    public String getDescription()
    {
        return "An application links entity type used for storing the relationship with a local " +
                "application entity like" +
                "            a JIRA project or Confluence space with a similar entity in the Remote App";
    }

    @Override
    public Schema getSchema()
    {
        return DocumentBasedSchema.builder("entity-type")
                .setPlugin(plugin)
                .setName(getName())
                .setDescription(getDescription())
                .build();
    }

    @Override
    public void validate(Element element, URI registrationUrl, String username) throws PluginParseException
    {
    }

    @Override
    public void generatePluginDescriptor(Element descriptorElement, Element pluginDescriptorRoot)
    {
    }
}
