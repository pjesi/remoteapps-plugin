package com.atlassian.labs.remoteapps.plugin.module.jira.projecttab;

import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.labs.remoteapps.plugin.integration.plugins.DescriptorToRegister;
import com.atlassian.labs.remoteapps.plugin.integration.plugins.DynamicDescriptorRegistration;
import com.atlassian.labs.remoteapps.plugin.module.ConditionProcessor;
import com.atlassian.labs.remoteapps.plugin.module.ContainingRemoteCondition;
import com.atlassian.labs.remoteapps.plugin.module.IFrameParams;
import com.atlassian.labs.remoteapps.plugin.module.IFrameRenderer;
import com.atlassian.labs.remoteapps.plugin.module.page.IFrameContext;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.util.concurrent.NotNull;
import org.dom4j.Element;

import java.net.URI;

import static com.atlassian.labs.remoteapps.spi.util.Dom4jUtils.getRequiredAttribute;
import static com.atlassian.labs.remoteapps.spi.util.Dom4jUtils.getRequiredUriAttribute;

/**
 * A remote project tab that loads is contents from an iframe
 */
public class ProjectTabPageModuleDescriptor extends AbstractModuleDescriptor<Void>
{
    private final IFrameRenderer iFrameRenderer;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final DynamicDescriptorRegistration dynamicDescriptorRegistration;
    private final ConditionProcessor conditionProcessor;
    private Element descriptor;
    private URI url;

    public ProjectTabPageModuleDescriptor(IFrameRenderer iFrameRenderer,
            JiraAuthenticationContext jiraAuthenticationContext,
            DynamicDescriptorRegistration dynamicDescriptorRegistration,
            ConditionProcessor conditionProcessor)
    {
        this.iFrameRenderer = iFrameRenderer;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.dynamicDescriptorRegistration = dynamicDescriptorRegistration;
        this.conditionProcessor = conditionProcessor;
    }

    @Override
    public Void getModule()
    {
        return null;
    }

    @Override
    public void init(@NotNull Plugin plugin, @NotNull Element element) throws PluginParseException
    {
        super.init(plugin, element);
        this.descriptor = element;
        this.url = getRequiredUriAttribute(element, "url");
    }

    @Override
    public void enabled()
    {
        super.enabled();
        final String panelName = getRequiredAttribute(descriptor, "name");

        Element desc = descriptor.createCopy();
        String moduleKey = "project-tab-" + getRequiredAttribute(descriptor, "key");
        Condition condition = conditionProcessor.process(descriptor, desc, getPluginKey(), "#" + moduleKey + "-remote-condition-panel");
        if (condition instanceof ContainingRemoteCondition)
        {
            moduleKey += "-remote-condition";
        }
        desc.addAttribute("key", moduleKey);
        desc.addElement("label").setText(panelName);
        desc.addAttribute("class", IFrameProjectTab.class.getName());

        ProjectTabPanelModuleDescriptor moduleDescriptor = createDescriptor(moduleKey,
                desc, new IFrameParams(descriptor), condition);
        dynamicDescriptorRegistration.registerDescriptors(getPlugin(), new DescriptorToRegister(moduleDescriptor));
    }

    private ProjectTabPanelModuleDescriptor createDescriptor(
            final String key,
            final Element desc,
            final IFrameParams iFrameParams, final Condition condition)
    {
        try
        {
            ProjectTabPanelModuleDescriptor descriptor = new FixedProjectTabPanelModuleDescriptor(
                    jiraAuthenticationContext, new ModuleFactory()
            {
                @Override
                public <T> T createModule(String name, ModuleDescriptor<T> moduleDescriptor) throws PluginParseException
                {

                    return (T) new IFrameProjectTab(
                            new IFrameContext(getPluginKey() , url, key, iFrameParams),
                            iFrameRenderer, condition);
                }
            });

            descriptor.init(getPlugin(), desc);
            return descriptor;
        }
        catch (Exception ex)
        {
            throw new PluginParseException(ex);
        }
    }
}
