<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <servlet-filter key="requestIdSetter" class="com.atlassian.labs.remoteapps.plugin.util.http.bigpipe.RequestIdSettingFilter"
                    location="after-encoding">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="iframe-resources" class="com.atlassian.labs.remoteapps.plugin.iframe.StaticResourcesFilter"
            location="after-encoding">
        <url-pattern>/remoteapps/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="universal-binary-dispatching" class="bean:ubDispatchFilter"
                    location="after-encoding" weight="1000">
        <url-pattern>/app/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="api-scoping" class="com.atlassian.labs.remoteapps.plugin.module.permission.ApiScopingFilter"
            location="before-decoration">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet-filter key="oauth-2lo-authentication" class="com.atlassian.labs.remoteapps.plugin.module.oauth.OAuth2LOFilter"
            location="after-encoding" weight="2000">
        <url-pattern>/*</url-pattern>
    </servlet-filter>

    <servlet key="remoteApps2LORedirectingServlet"
             name="Remote Apps 2-Legged OAuth Signing and Redirecting Servlet"
             class="com.atlassian.labs.remoteapps.plugin.module.util.redirect.RedirectServlet">
        <description>Redirects to Remote Apps, using 2-legged OAuth to sign the outgoing request.  Request parameters are included in the redirect.</description>
        <url-pattern>/redirect/oauth</url-pattern>
    </servlet>
    <servlet key="remoteAppsPermanentRedirectingServlet"
             name="Remote Apps Permanent Redirecting Servlet"
             class="com.atlassian.labs.remoteapps.plugin.module.util.redirect.RedirectServlet">
        <description>
            Redirects to a Pemote App resource, usually an image, with a permanent redirect, but
            doesn't sign the request.  Request parameters are included in the redirect.
        </description>
        <url-pattern>/redirect/permanent</url-pattern>
    </servlet>

    <web-resource key="iframe-host">
        <resource type="download" name="easyXDM.js" location="/js/iframe/easyXDM.js" />
        <resource type="download" name="ra-host.js" location="/js/iframe/host/ra-host.js" />
    </web-resource>

    <web-resource key="iframe-requirements">
        <context>remoteapps-iframe</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <web-resource key="dialog">
        <resource type="download" name="dialog.js" location="js/dialog/dialog.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <web-resource key="remote-condition">
        <resource type="download" name="remote-condition.js" location="js/condition/remote-condition.js" />
        <dependency>${project.groupId}.${project.artifactId}:big-pipe</dependency>
    </web-resource>

    <web-resource key="images">
        <resource type="download" name="images/" location="images/" />
    </web-resource>

    <web-resource key="dialog-page-resource">
        <resource type="download" name="dialog-binder.js" location="/js/dialog/binder.js" />
        <context>atl.general</context>
        <context>atl.admin</context>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>${project.groupId}.${project.artifactId}:dialog</dependency>
    </web-resource>

    <web-resource key="smoke-test">
        <resource type="download" content-type="application/xml" name="atlassian-remote-app.xml" location="/remoteapps-smoke-test/atlassian-remote-app.xml" />
        <resource type="download" name="general-page.html" content-type="text/html" location="/remoteapps-smoke-test/general-page.html" />
        <transformation extension="xml">
            <transformer key="baseUrlTransformer"/>
        </transformation>
    </web-resource>

    <web-resource key="schema-xsl">
        <resource type="download" content-type="application/xml" name="xs3p.xsl" location="/xsd/xs3p.xsl"/>
    </web-resource>

    <web-resource-transformer key="baseUrlTransformer" class="com.atlassian.labs.remoteapps.plugin.integration.smoketest.BaseUrlTransformer"/>

    <!-- big pipe stuff -->
    <web-panel key="requestIdMetaTag" class="com.atlassian.labs.remoteapps.plugin.util.http.bigpipe.RequestIdWebPanel" location="atl.header"/>

    <web-resource key="big-pipe">
        <resource type="download" name="big-pipe.js" location="js/bigpipe/big-pipe.js" />
        <dependency>com.atlassian.auiplugin:ajs</dependency>
    </web-resource>

    <component key="httpContentRetriever" class="com.atlassian.labs.remoteapps.plugin.util.http.CachingHttpContentRetriever"
               public="true" interface="com.atlassian.labs.remoteapps.plugin.util.http.HttpContentRetriever" />

    <component key="startableForPlugins" class="com.atlassian.labs.remoteapps.plugin.loader.StartableForPlugins" public="true"
               interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="productEventPublisher" class="com.atlassian.labs.remoteapps.spi.event.product.ProductEventPublisher"
               public="true" interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <component key="httpResourceMounter" class="com.atlassian.labs.remoteapps.plugin.loader.LocalHttpResourceMounterServiceFactory" interface="com.atlassian.labs.remoteapps.api.service.HttpResourceMounter" public="true" />

    <component key="remotappsService" class="com.atlassian.labs.remoteapps.plugin.DefaultRemoteAppsService"
               interface="com.atlassian.labs.remoteapps.spi.RemoteAppsService" public="true" />

    <component key="bundleLocator" class="com.atlassian.labs.remoteapps.host.common.util.BundleContextBundleLoader" />
    <component key="permissionsReader" class="com.atlassian.labs.remoteapps.host.common.descriptor.DescriptorPermissionsReader" />

    <component-import key="templateRenderer" interface="com.atlassian.templaterenderer.TemplateRenderer"/>
    <component-import key="servletModuleManager" interface="com.atlassian.plugin.servlet.ServletModuleManager" />
    <component-import key="webInterfaceManager" interface="com.atlassian.plugin.web.WebInterfaceManager" />
    <component-import key="pluginRetrievalService" interface="com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService" />
    <component-import key="oauthConsumerService" interface="com.atlassian.oauth.consumer.ConsumerService" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />
    <component-import key="pluginController" interface="com.atlassian.plugin.PluginController" />
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="serviceProviderConsumerStore" interface="com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore" />
    <component-import key="applicationLinkService" interface="com.atlassian.applinks.spi.link.MutatingApplicationLinkService" />
    <component-import key="authenticationConfigurationManager" interface="com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="authenticationListener" interface="com.atlassian.sal.api.auth.AuthenticationListener" />
    <component-import key="authenticationController" interface="com.atlassian.sal.api.auth.AuthenticationController" />
    <component-import key="transactionTemplate" interface="com.atlassian.sal.api.transaction.TransactionTemplate" />
    <component-import key="pluginEventManager" interface="com.atlassian.plugin.event.PluginEventManager" />
    <component-import key="pluginAccessor" interface="com.atlassian.plugin.PluginAccessor" />
    <component-import key="webResourceManager" interface="com.atlassian.plugin.webresource.WebResourceManager" />
    <component-import key="eventPublisher" interface="com.atlassian.event.api.EventPublisher" />
    <component-import key="i18nResolver" interface="com.atlassian.sal.api.message.I18nResolver" />
    <component-import key="pluginSettingsFactory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" />
    <component-import key="typeAccessor" interface="com.atlassian.applinks.spi.util.TypeAccessor" />
    <component-import key="webResourceUrlProvider" interface="com.atlassian.plugin.webresource.WebResourceUrlProvider" />
    <component-import key="moduleFactory" interface="com.atlassian.plugin.module.ModuleFactory" />

    <!-- Cross-product API Scopes -->
    <plugin-permission key="read_app_links" name="Read Application Links"
        class="com.atlassian.labs.remoteapps.plugin.product.ReadAppLinksScope">
        <description>Allows a Remote App to retrieve the host application's configured application links and entity links.</description>
    </plugin-permission>

    <plugin-permission key="modify_app_link" name="Modify Owned App Link"
        class="com.atlassian.labs.remoteapps.plugin.product.ModifyAppLinkScope">
        <description>Allows a Remote App to modify the details of its own configured Application Link.</description>
    </plugin-permission>

    <!-- speakeasy integration -->
    <commonjs key="speakeasy-commonjs" location="/js/speakeasy" />

    <web-resource key="speakeasy-integration">
        <context>speakeasy.user-profile</context>
        <dependency>com.atlassian.labs.remoteapps-plugin:remoteapps_installer</dependency>
    </web-resource>

    <web-item key="speakeasy-install" section="speakeasy.user-profile/actions" weight="100">
        <label>Install Remote App</label>
        <link linkId="rp-install" />
        <condition class="com.atlassian.labs.remoteapps.plugin.integration.speakeasy.CanInstallRemoteAppsCondition" />
    </web-item>

    <!-- universal binary services -->
    <component key="localSignedRequestHandler" interface="com.atlassian.labs.remoteapps.api.service.SignedRequestHandler"
               class="com.atlassian.labs.remoteapps.plugin.service.LocalSignedRequestHandlerServiceFactory" public="true" />

    <component key="requestContext" interface="com.atlassian.labs.remoteapps.api.service.RequestContext"
               class="com.atlassian.labs.remoteapps.host.common.service.RequestContextServiceFactory" public="true" />

    <component key="requestKiller"
               class="com.atlassian.labs.remoteapps.host.common.service.http.RequestKiller" />

    <component key="asyncHttpClient" interface="com.atlassian.labs.remoteapps.api.service.http.HttpClient"
               class="com.atlassian.labs.remoteapps.host.common.service.http.DefaultHttpClient" public="true" />

    <component key="httpClient" interface="com.atlassian.labs.remoteapps.api.service.http.HostHttpClient"
               class="com.atlassian.labs.remoteapps.host.common.service.http.HostHttpClientServiceFactory" public="true" />

    <component key="hostXmlRpcClient" interface="com.atlassian.labs.remoteapps.api.service.http.HostXmlRpcClient"
               class="com.atlassian.labs.remoteapps.host.common.service.http.HostXmlRpcClientServiceFactory" public="true" />

    <component key="localEmailSender" interface="com.atlassian.labs.remoteapps.api.service.EmailSender"
               class="com.atlassian.labs.remoteapps.plugin.service.LocalEmailSenderServiceFactory" public="true" />

    <applinks-application-type name="Remote Plugin Container" key="remote-plugin-container-type"
                                   class="com.atlassian.labs.remoteapps.plugin.module.applinks.RemotePluginContainerApplicationTypeImpl"
                                   interface="com.atlassian.labs.remoteapps.spi.applinks.RemotePluginContainerApplicationType">
            <manifest-producer class="com.atlassian.labs.remoteapps.plugin.module.applinks.RemotePluginContainerManifestProducer"/>
    </applinks-application-type>

    <module-type key="described-module-type" class="com.atlassian.labs.remoteapps.plugin.integration.plugins.DescribedModuleTypeModuleDescriptor" />

    <!-- remote extension points -->

    <described-module-type key="remote-plugin-container" name="Remote Plugin Container"
                               class="com.atlassian.labs.remoteapps.plugin.module.applinks.RemotePluginContainerModuleDescriptor">
        <description>The server that is hosting this plugin</description>
        <optional-permissions>
            <permission>create_oauth_link</permission>
        </optional-permissions>
    </described-module-type>

    <described-module-type key="webhook" name="Web Hook" class="com.atlassian.labs.remoteapps.plugin.module.webhook.WebHookModuleDescriptor"
                           schema-transformer-class="com.atlassian.labs.remoteapps.plugin.module.webhook.WebHookSchemaFactory">
        <description>Registration for a web hook callback from an internal event</description>
    </described-module-type>

    <described-module-type key="general-page" class="com.atlassian.labs.remoteapps.plugin.module.page.GeneralPageModuleDescriptor">
        <description>A non-admin general page decorated by the application, with a link in a globally-accessible place</description>
    </described-module-type>

    <described-module-type key="admin-page" name="Administration Page" class="com.atlassian.labs.remoteapps.plugin.module.page.AdminPageModuleDescriptor">
        <description>An admin page decorated in the admin section, with a link in the admin menu</description>
    </described-module-type>

    <described-module-type key="configure-page" class="com.atlassian.labs.remoteapps.plugin.module.page.ConfigurePageModuleDescriptor" max-occurs="1">
        <description>The configuration page for the app, decorated in the admin section, with a link in the admin menu and a configure link in the Plugin Manager</description>
    </described-module-type>

    <described-module-type key="dialog-page" class="com.atlassian.labs.remoteapps.plugin.module.page.dialog.DialogPageModuleDescriptor">
        <description>Loads a remote URL (iframe-wrapped) in an AUI Dialog</description>
    </described-module-type>

    <described-module-type key="plugin-permission" name="Plugin Permission"
                                   class="com.atlassian.labs.remoteapps.spi.permission.PermissionModuleDescriptor">
        <description>A permission that needs to be requested by a plugin in order to execute certain functionality</description>
        <required-permissions>
            <permission>execute_java</permission>
        </required-permissions>
    </described-module-type>

    <plugin-permission key="create_oauth_link" name="Create OAuth Link">
        <description>Ability to create an incoming OAuth link that will allow trusted access</description>
    </plugin-permission>

    <plugin-permission key="execute_java" name="Execute Java">
        <description>Ability to execute arbitrary Java code on the Atlassian server</description>
        <!-- todo: add scanning -->
    </plugin-permission>

    <!-- product-specific modules included via xslt build process -->

</atlassian-plugin>
