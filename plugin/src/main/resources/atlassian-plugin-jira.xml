<application key="jira">
    <component key="jiraProductAccessor" class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraProductAccessor"/>

    <web-item key="general-dropdown-item" section="system.top.navigation.bar" weight="100">
        <label>Remote Apps</label>
        <link linkId="general_dropdown_linkId"/>
        <condition class="com.atlassian.labs.remoteapps.plugin.module.jira.HasGeneralPagesCondition" />
    </web-item>

    <web-section key="remoteapps.general" location="general_dropdown_linkId">
    </web-section>

    <!-- remote module generators -->

    <component key="issue-panel-page-generator" class="com.atlassian.labs.remoteapps.plugin.module.jira.issuepanel.ViewIssuePanelModuleGenerator" />
    <component key="issue-tab-page-generator" class="com.atlassian.labs.remoteapps.plugin.module.jira.issuetab.IssueTabPageModuleGenerator" />
    <component key="project-tab-page-generator" class="com.atlassian.labs.remoteapps.plugin.module.jira.projecttab.ProjectTabModuleGenerator" />
    <component key="profile-page-generator" class="com.atlassian.labs.remoteapps.plugin.module.page.jira.JiraProfileTabModuleGenerator" />
    <component key="search-request-view-page-generator" class="com.atlassian.labs.remoteapps.plugin.module.jira.searchrequestview.SearchRequestViewModuleGenerator" />

    <!-- api scopes -->
    <plugin-permission key="read_users_and_groups" name="Read Users and Groups"
        class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraReadUsersAndGroupsScope">
        <description>View users and groups</description>
    </plugin-permission>
    <plugin-permission key="browse_projects" name="Browse Projects"
        class="com.atlassian.labs.remoteapps.plugin.product.jira.BrowseProjectsScope">
        <description>
            Permission to browse projects, search issues, and view individual issues (except issues that have been
            restricted via Issue Security)
        </description>
    </plugin-permission>
    <plugin-permission key="create_issues" name="Create Issues"
        class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraCreateIssuesScope">
        <description>
            Permission to create issues in the project. (Note that the Create Attachments
            permission is required in order to create attachments.) Includes the ability to
            create sub-tasks (if sub-tasks are enabled).
        </description>
    </plugin-permission>
    <plugin-permission key="edit_issues" name="Edit Issues"
        class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraEditIssuesScope">
        <description>
            Permission to edit issues (excluding the  'Due Date' field - see the Schedule
            Issues permission). Includes the ability to convert issues to sub-tasks and vice
            versa (if sub-tasks are enabled). Note that the Delete Issue permission is
            required in order to delete issues.
        </description>
    </plugin-permission>
    <plugin-permission key="resolve_issues" name="Resolve Issues"
        class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraResolveIssuesScope">
        <description>
            Permission to resolve and reopen issues. This also includes the ability to set
            the 'Fix For version' field for issues. Also see the Close Issues permission.
        </description>
    </plugin-permission>

    <component key="WebHookProvider" class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraWebHookProvider" />

    <component key="EventSerializerFactory" class="com.atlassian.labs.remoteapps.plugin.product.jira.webhook.JiraEventSerializerFactory" />

    <component key="WebSudoElevator" class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraWebSudoElevator" />

    <component key="host" class="com.atlassian.labs.remoteapps.plugin.module.ServletContextThreadLocalIFrameHost" />
    <component key="restAccessor" class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraRestBeanMarshaler" />

    <rest key="rest" path="/remoteapps" version="1">
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>INCLUDE</dispatcher>
        <package>com.atlassian.labs.remoteapps.plugin.rest</package>
    </rest>

    <!-- Handles loading big pipe resources because web items in jira
        can't contribute web resources due to how JIRA renders
     -->
    <jira-footer key="bigPipeFooter" class="com.atlassian.labs.remoteapps.plugin.product.jira.JiraBigPipeFooter" />

    <component-import key="jiraAuthenticationContext" interface="com.atlassian.jira.security.JiraAuthenticationContext" />
    <component-import key="jiraUserManager" interface="com.atlassian.jira.user.util.UserManager" />
    <component-import key="InternalWebSudoManager" interface="com.atlassian.jira.security.websudo.InternalWebSudoManager" />
    <component-import key="SearchRequestViewBodyWriterUtil" interface="com.atlassian.jira.issue.views.util.SearchRequestViewBodyWriterUtil" />
    <component-import key="searchRequestURLHandler" interface="com.atlassian.jira.plugin.searchrequestview.SearchRequestURLHandler" />
    <component-import key="mailQueue" interface="com.atlassian.mail.queue.MailQueue" />
    <component-import key="projectRoleManager" interface="com.atlassian.jira.security.roles.ProjectRoleManager" />
    <component-import key="jiraBaseUrls" interface="com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls" />

    <!-- plugin points -->
    <described-module-type key="profile-page" name="User Profile Page" class="com.atlassian.labs.remoteapps.plugin.module.page.jira.JiraProfileTabModuleDescriptor">
        <description>A user profile page decorated as a user profile tab</description>
    </described-module-type>

    <described-module-type key="remote-search-request-view" class="com.atlassian.labs.remoteapps.plugin.module.jira.searchrequestview.RemoteSearchRequestViewModuleDescriptor">
        <description>
            A search request view that redirects to the url with found issue keys as the
            'issues' query parameter
        </description>
    </described-module-type>

    <described-module-type key="project-tab-page" class="com.atlassian.labs.remoteapps.plugin.module.jira.projecttab.ProjectTabPageModuleDescriptor">
        <description>
            A remote page decorated as its own JIRA project tab
        </description>
    </described-module-type>

    <described-module-type key="issue-tab-page" class="com.atlassian.labs.remoteapps.plugin.module.jira.issuetab.IssueTabPageModuleDescriptor">
        <description>
            A remote page decorated as its own JIRA issue tab but not included in All tab as it has no individual actions
        </description>
    </described-module-type>

    <described-module-type key="issue-panel-page" class="com.atlassian.labs.remoteapps.plugin.module.jira.issuepanel.IssuePanelPageModuleDescriptor">
        <description>
            A remote page decorated as a web panel on the view issue page
        </description>
    </described-module-type>

    <!-- JIRA services -->
    <component key="jiraComponentClient" interface="com.atlassian.jira.rest.client.p3.JiraComponentClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraComponentClientServiceFactory"
               public="true"/>
    <component key="jiraProjectClient" interface="com.atlassian.jira.rest.client.p3.JiraProjectClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraProjectClientServiceFactory"
               public="true"/>
    <component key="jiraVersionClient" interface="com.atlassian.jira.rest.client.p3.JiraVersionClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraVersionClientServiceFactory"
               public="true"/>
    <component key="jiraUserClient" interface="com.atlassian.jira.rest.client.p3.JiraUserClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraUserClientServiceFactory"
               public="true"/>
    <component key="jiraMetadataClient" interface="com.atlassian.jira.rest.client.p3.JiraMetadataClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraMetadataClientServiceFactory"
               public="true"/>
    <component key="jiraSearchClient" interface="com.atlassian.jira.rest.client.p3.JiraSearchClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraSearchClientServiceFactory"
               public="true"/>
    <component key="jiraIssueClient" interface="com.atlassian.jira.rest.client.p3.JiraIssueClient"
               class="com.atlassian.labs.remoteapps.host.common.service.jira.JiraIssueClientServiceFactory"
               public="true"/>
</application>







