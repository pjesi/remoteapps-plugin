package com.atlassian.labs.remoteapps.test.webhook;

public interface WebHookBody
{
    String find(String expression) throws Exception;
}
